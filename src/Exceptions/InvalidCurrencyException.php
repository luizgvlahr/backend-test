<?php

declare(strict_types = 1);

namespace Opeepl\BackendTest\Exceptions;

use Exception;

class InvalidCurrencyException extends Exception
{
    public function __construct()
    {
        $message = "Invalid currency code!";
        parent::__construct($message);
    }
}
