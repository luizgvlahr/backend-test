<?php

declare(strict_types = 1);

namespace Opeepl\BackendTest\Exceptions;

use Exception;

class InvalidAmountException extends Exception
{
    public function __construct()
    {
        $message = "Invalid amount! Please, inform a amount greater than 0 (zero).";
        parent::__construct($message);
    }
}
