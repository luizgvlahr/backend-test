<?php
namespace Opeepl\BackendTest\Service;

use Opeepl\BackendTest\Client\Http\HttpClient;
use Opeepl\BackendTest\Client\Http\PHPHttpClient;
use Opeepl\BackendTest\Client\Http\ExchangeRateClient;
use Opeepl\BackendTest\Exceptions\InvalidAmountException;
use Opeepl\BackendTest\Exceptions\InvalidCurrencyException;
use Opeepl\BackendTest\Client\ExchangeRate\ERAExchangeRateClient;

/**
 * Main entrypoint for this library.
 */
class ExchangeRateService
{

    /** @var ExchangeRateClient */
    private $exchangeClient;

    /** @var HttpClient */
    private $httpClient;

    /** @var array<string> */
    private $currencies;
    
    /** @var array<string,float> */
    private $rates;

    public function __construct()
    {
        $this->httpClient = new PHPHttpClient();
        $this->exchangeClient = new ERAExchangeRateClient($this->httpClient);

        $this->rates = $this->exchangeClient->getRates();
        $this->currencies = $this->getCurrencies($this->rates);
    }

    /**
     * Return all supported currencies
     *
     * @return array<string>
     */
    public function getSupportedCurrencies(): array
    {
        return $this->currencies;
    }

    /**
     * Given the $amount in $fromCurrency, it returns the corresponding amount in $toCurrency.
     *
     * @param int $amount
     * @param string $fromCurrency
     * @param string $toCurrency
     * @return int
     */
    public function getExchangeAmount(int $amount, string $fromCurrency, string $toCurrency): int
    {
        $this->validateCurrency($fromCurrency);
        $this->validateCurrency($toCurrency);
        $this->validateAmount($amount);

        $response = $this->exchangeClient->getConvertedAmount($amount, $fromCurrency, $toCurrency);

        return $response;
    }

    /** @return array<string> */
    private function getCurrencies(array $rates)
    {
        $currencies = array_keys($this->rates);
        array_push($currencies, $this->exchangeClient::BASE_CURRENCY);
        return $currencies;
    }

    private function validateCurrency(string $currency): void
    {
        if (!in_array($currency, $this->currencies)) {
            throw new InvalidCurrencyException();
        }
    }

    private function validateAmount(int $amount): void
    {
        if (empty($amount) || is_nan($amount) || $amount <= 0) {
            throw new InvalidAmountException();
        }
    }
}
