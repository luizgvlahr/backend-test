<?php

declare(strict_types = 1);

namespace Opeepl\BackendTest\Client\ExchangeRate;

use Opeepl\BackendTest\Client\Http\HttpClient;
use Opeepl\BackendTest\Client\ExchangeRate\ExchangeRateClient;

class ERAExchangeRateClient implements ExchangeRateClient
{
    const BASE_URL = 'https://api.exchangeratesapi.io/';
    const BASE_CURRENCY = 'EUR';

    private $httpClient;

    public function __construct(HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function getRates(): array
    {
        $url = self::BASE_URL . 'latest' . '?base=' . self::BASE_CURRENCY;
        $response = $this->httpClient->get($url);
        return $response["rates"];
    }

    public function getConvertedAmount(int $amount, string $fromCurrency, string $toCurrency): int
    {
        if ($fromCurrency === $toCurrency) {
            return $amount;
        }

        $url = self::BASE_URL . 'latest' . '?base=' . $fromCurrency . '&symbols=' . $toCurrency;
        $response = $this->httpClient->get($url);
        
        $rate = $response["rates"][$toCurrency];

        return $this->convertAmount($amount, $rate);
    }

    private function convertAmount(int $amount, float $rate): int
    {
        $decimalAmount = round($rate * $amount, 0, PHP_ROUND_HALF_DOWN);
        return intval($decimalAmount);
    }
}
