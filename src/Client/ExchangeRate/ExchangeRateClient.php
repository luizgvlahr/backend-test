<?php

declare(strict_types = 1);

namespace Opeepl\BackendTest\Client\ExchangeRate;

interface ExchangeRateClient
{
    public function getRates(): array;
    public function getConvertedAmount(int $amount, string $fromCurrency, string $toCurrency): int;
}
