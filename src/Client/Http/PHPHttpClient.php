<?php

declare(strict_types = 1);

namespace Opeepl\BackendTest\Client\Http;

class PHPHttpClient implements HttpClient
{
    /** return array<mixed> */
    public function get(string $url)
    {
        $encodedResponse = file_get_contents($url);
        $response = json_decode($encodedResponse, true);

        return $response;
    }
}
