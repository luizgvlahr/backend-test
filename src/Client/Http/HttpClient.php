<?php

declare(strict_types = 1);

namespace Opeepl\BackendTest\Client\Http;

interface HttpClient
{
    /** return array<mixed> */
    public function get(string $url);
}
