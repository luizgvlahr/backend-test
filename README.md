## Introduction

Test realized as part of recruitment process for Opeepl

## Requirements

[x] We want to keep the number of calls to the API to a minimum - also across multiple php processes.

[x] We want it to be easy to switch to another exchange rate API.

[x] We want to be able to support multiple APIs AT THE SAME TIME. For instance adding an API that has cryptocurrency exchange rates.

[] We want to be able to support exchanging between currencies defined in separate exchange rate APIs. What are the requirements for doing that? **Not sure I understood this one**
